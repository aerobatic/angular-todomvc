Angular TodoMVC Example
============================

This repository was forked from [AngularClass/NG6-todomvc-starter](https://github.com/AngularClass/NG6-todomvc-starter) to be used as a demonstration of the [Aerobatic npm build pipeline](https://www.aerobatic.com/docs/automated-builds#npm).

You can see a live demo at: [https://angular-todomvc.aerobatic.io](https://angular-todomvc.aerobatic.io).
